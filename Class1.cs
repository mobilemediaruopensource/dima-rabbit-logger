﻿using System;
using RabbitMQ.Client;
using System.Text;
using Newtonsoft.Json;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace Checkaso.Tools
{
    public interface ILoggerSettings
    {
        string getUser();
        string getPassword();
        string getHost();
        string getPort();
    }

    public interface IDimaRabbit
    {
        void SendDefault(string topic, string name, string token, string data);
        void SendDefault(string[] topic);
        void SendDefault(string[] topic, string token);
        void SendDefault(string topic, string token, string data);
        void SendJsonObject(string customJsonObject);
        void SetChannel(string exchange);
    }

    public class LoggerSettings : ILoggerSettings
    {
        public string User;
        public string Password;
        public string Host;
        public string Port;

        public LoggerSettings(string user, string password, string host, string port)
        {
            User = user;
            Password = password;
            Host = host;
            Port = port;
        }

        public string getUser()
        {
            return User;
        }

        public string getPassword()
        {
            return Password;
        }

        public string getHost()
        {
            return Host;
        }

        public string getPort()
        {
            return Port;
        }
    }

    public class DefaultMessage
    {
        public string topic;
        public string name;
        public string token;
        public string data;

        public DefaultMessage(string _topic, string _name, string _token, string _data)
        {
            topic = _topic;
            name = _name;
            token = _token;
            data = _data;
        }
    }

    public class DefaultSecondMessage
    {
        public string topic;
        public string token;
        public string data;

        public DefaultSecondMessage(string _topic, string _token, string _data)
        {
            topic = _topic;
            token = _token;
            data = _data;
        }
    }

    public class DimaLogger : IDimaRabbit
    {
        private string _user;
        private string _password;
        private string _host;
        private string _port;
        private string _exchange = "logs";

        public DimaLogger(string user, string password, string host, string port)
        {
            _user = user;
            _password = password;
            _host = host;
            _port = port;
        }

        public void SendDefault(string topic, string name, string token, string data)
        {
            var defaultObject = new DefaultMessage(topic, name, token, data);
            var json = JsonConvert.SerializeObject(defaultObject);
            var body = Encoding.UTF8.GetBytes(json);
            sendLog(body);
        }

        public void SendDefault(string topic, string token, string data)
        {
            var defaultObject = new DefaultSecondMessage(topic, token, data);
            var json = JsonConvert.SerializeObject(defaultObject);
            var body = Encoding.UTF8.GetBytes(json);
            sendLog(body);
        }

        public void SendDefault(string[] topic)
        {
            var topicStringBuilder = new StringBuilder();

            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var token = Guid.NewGuid().ToString();

            int count = 0;
            
            foreach (var items in topic)
            {
                if (count == 1)
                {
                    topicStringBuilder.Append(env);
                    topicStringBuilder.Append(".");
                }

                topicStringBuilder.Append(items);

                if (count + 1 != topic.Length)
                {
                    topicStringBuilder.Append(".");
                }

                count++;
            }
            
            var defaultObject = new DefaultSecondMessage(topicStringBuilder.ToString(), token, null);
            var json = JsonConvert.SerializeObject(defaultObject);
            var body = Encoding.UTF8.GetBytes(json);
            sendLog(body);
        }
        public void SendDefault(string[] topic, string token)
        {
            var topicStringBuilder = new StringBuilder();

            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            int count = 0;
            
            foreach (var items in topic)
            {
                if (count == 1)
                {
                    topicStringBuilder.Append(env);
                    topicStringBuilder.Append(".");
                }

                topicStringBuilder.Append(items);

                if (count + 1 != topic.Length)
                {
                    topicStringBuilder.Append(".");
                }

                count++;
            }
            
            var defaultObject = new DefaultSecondMessage(topicStringBuilder.ToString(), token, null);
            var json = JsonConvert.SerializeObject(defaultObject);
            var body = Encoding.UTF8.GetBytes(json);
            sendLog(body);
        }
        
        public void SendJsonObject(string customJsonObject)
        {
            var body = Encoding.UTF8.GetBytes(customJsonObject);
            sendLog(body);
        }

        private void sendLog(byte[] body)
        {
            var factory = new ConnectionFactory
            {
                Uri = new Uri("amqp://" + _user + ":" + _password + "@" + _host + ":" + _port)
            };

            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.ExchangeDeclare(exchange: _exchange, type: "fanout");

            channel.BasicPublish(exchange: "logs_exchange", routingKey: "", basicProperties: null, body: body);
            channel.Close();
        }

        public void SetChannel(string exchange)
        {
            _exchange = exchange;
        }
    }
}

public static class ServiceProviderExtensions
{
    public static void AddDimaLogger(this IServiceCollection services, IConfiguration config)
    {
        var user = config.GetConnectionString("RabbitUser");
        var password = config.GetConnectionString("RabbitPassword");
        var host = config.GetConnectionString("RabbitHost");
        var port = config.GetConnectionString("RabbitPort");

        services.AddSingleton<Checkaso.Tools.IDimaRabbit>(new Checkaso.Tools.DimaLogger(user, password, host, port));
    }

    public static void AddDimaLogger(this IServiceCollection services, Checkaso.Tools.ILoggerSettings config)
    {
        var user = config.getUser();
        var password = config.getPassword();
        var host = config.getHost();
        var port = config.getPort();

        services.AddSingleton<Checkaso.Tools.IDimaRabbit>(new Checkaso.Tools.DimaLogger(user, password, host, port));
    }
}